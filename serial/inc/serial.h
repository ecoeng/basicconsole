/*
 * serial.h
 * Author: Satya Kishan Ungarala
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <stdint.h>
#include "stm32g0xx_hal.h"

#include "FreeRTOS.h"
#include "task.h"

#define SERIAL_EVENT_END_OF_TRANSMIT    (1 << 0)
#define SERIAL_EVENT_END_OF_RECEIVE     (1 << 1)
#define SERIAL_EVENT_TRANSMIT_START     (1 << 2)

#define SERIAL_TX_BUFFER_SIZE 256
#define SERIAL_RX_BUFFER_SIZE 256

enum{
	RUNNING = 0,
};

typedef struct {
	uint8_t state;
	uint8_t txbuf[SERIAL_TX_BUFFER_SIZE];
	uint8_t rxbuf[SERIAL_RX_BUFFER_SIZE];
	uint8_t txhead;//change the data type if the buffer size is more than the max value of present data type
	uint8_t txtail;//change the data type if the buffer size is more than the max value of present data type
	uint8_t rxhead;
	uint8_t rxtail;

	uint8_t serialPrompt;
	uint8_t cmdBuf[20];
	uint8_t cmdLen;
	void (*cmdNotify_p)(void* priv, uint8_t* cmdBuffer, uint8_t cmdLength);
	void *cmdNotify_priv;

	//task meta data
	TaskHandle_t Task_Handle;
	uint32_t Task_Handle_Buffer[64U];
	StaticTask_t Task_Handle_Data;

	//Uart driver pointer
	UART_HandleTypeDef* uart_p;

}serial_t;

int serial_init(serial_t* dev_p);
void serial_task(void* priv);
int serial_send(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize);
int serial_read(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize);
int updateRxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize);
void EndofTransmit(serial_t* dev_p);


#endif /* SERIAL_H_ */
