/*
 * serial.c
 * Author: Satya Kishan Ungarala
 */
 
#include "serial.h"
#include <string.h>

static int s_updateTxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize);
static int s_readRxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize);

int serial_init(serial_t* dev_p){
	if(!dev_p)return -1;

	dev_p->cmdLen = 0;
	memset(dev_p->cmdBuf, 0 , sizeof(dev_p->cmdBuf));

	dev_p->serialPrompt = '>';

	dev_p->Task_Handle = xTaskCreateStatic(
			  serial_task,
	          "Serial_Task",
	          64U,
			  dev_p,
	          1U,
			  dev_p->Task_Handle_Buffer,
	          &(dev_p->Task_Handle_Data));

	  if(dev_p->Task_Handle == NULL)
	  {
	      //do something
	  }

	  return 0;
}

void serial_task(void* priv){
	serial_t* dev_p;
	uint8_t data = 0;
	uint32_t size = 0;
	uint8_t sendData[3] = "";
	uint32_t Task_Notification_Count = 0;

    for(;;)
    {
    	dev_p = (serial_t*) priv;
    	if(dev_p == NULL) return;

        Task_Notification_Count = ulTaskNotifyTake(pdTRUE, (TickType_t) 0xFFFF);
        //Task_Notification_Count = ulTaskNotifyTakeIndexed(0, pdTRUE, (TickType_t) 0xFFFF);

		switch(dev_p->state){

		case RUNNING:


			if(Task_Notification_Count)
			{
				//Process RX Buffer
				while(dev_p->rxtail != dev_p->rxhead){
					serial_read(dev_p, &data, 1);

					if((data == '\r') || (data == '\n')){
						sendData[0] = '\r';
						sendData[1] = '\n';
						sendData[2] = dev_p->serialPrompt;
						size = 3;

						if(dev_p->cmdNotify_p)dev_p->cmdNotify_p(dev_p->cmdNotify_priv, dev_p->cmdBuf, dev_p->cmdLen);
						memset(dev_p->cmdBuf, 0 , sizeof(dev_p->cmdBuf));
						dev_p->cmdLen = 0;

					}
					else if(data == '\b'){
						sendData[0] = '\b';
						sendData[1] = ' ';
						sendData[2] = '\b';
						size = 3;
						if((dev_p->cmdLen) > 0){
							(dev_p->cmdLen)--;
						}
					}
					else{
						sendData[0] = data;
						size = 1;
						dev_p->cmdBuf[dev_p->cmdLen] = data;
						(dev_p->cmdLen)++;
					}

					//HAL_UART_Transmit_IT(dev_p->uart_p, sendData, size);
					serial_send(dev_p, sendData, size);

				}


				//Process TX Buffer
				while(dev_p->txtail != dev_p->txhead){
					data = dev_p->txbuf[dev_p->txtail];
					size = 1;
					if(HAL_UART_Transmit_IT(dev_p->uart_p, &data, size) == HAL_OK){
						dev_p->txtail = (dev_p->txtail + size) % SERIAL_TX_BUFFER_SIZE;
					}
				}

			}

			break;
		default:
			break;

		}

    }
}

int serial_send(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize){
	int result = 0;
	if(dev_p == NULL)return -1;
	result = s_updateTxbuffer(dev_p, buffer, buffersize);
	xTaskNotifyGive(dev_p->Task_Handle);
	//xTaskNotifyGiveIndexed(dev_p->Task_Handle, 0);
	return result;
}

static int s_updateTxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize){
	int i = 0;
	if(dev_p == NULL)return -1;
	for(i=0; i<buffersize; i++){
		if(((dev_p->txhead + 1) % SERIAL_TX_BUFFER_SIZE) != dev_p->txtail){
			dev_p->txbuf[dev_p->txhead] = *(buffer + i);
			dev_p->txhead = (dev_p->txhead + 1) % SERIAL_TX_BUFFER_SIZE;
		}
		else{
			//buffer full
			return 0;
			break;
		}
	}
	return i;
}

int updateRxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize){
	int i = 0;
	if(dev_p == NULL)return -1;
	for(i=0; i<buffersize; i++){
		if(((dev_p->rxhead + 1) % SERIAL_RX_BUFFER_SIZE) != dev_p->rxtail){
			dev_p->rxbuf[dev_p->rxhead] = *(buffer + i);
			dev_p->rxhead = (dev_p->rxhead + 1) % SERIAL_RX_BUFFER_SIZE;
		}
		else{
			//buffer overflow
			break;
		}
	}
	return i;
}


int serial_read(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize){
	int result = 0;
	if(dev_p == NULL)return -1;
	result = s_readRxbuffer(dev_p, buffer, buffersize);
	return result;

}

static int s_readRxbuffer(serial_t* dev_p, uint8_t* buffer, uint8_t buffersize){
	int i = 0;
	if(dev_p == NULL)return -1;
	for(i=0; i<buffersize; i++){
		if((dev_p->rxtail) != dev_p->rxhead){
			*(buffer + i) = dev_p->rxbuf[dev_p->rxtail];
			dev_p->rxtail = (dev_p->rxtail + 1) % SERIAL_RX_BUFFER_SIZE;
		}
		else{
			return 0;
			//buffer empty
			break;
		}
	}
	return i;
}

//Call this in UART ISR
void EndofTransmit(serial_t* dev_p)//Callback function for "End of transmit" event.

{
	if(dev_p == NULL)return;
}


